﻿using AnsInterviewTest.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnsInterviewTest.Models
{
    public class PersonEditModel
    {
        private readonly Person person;

        private readonly IEnumerable<Colour> availableColours;

        /// <summary>
        /// Initialises a new instance of the <see cref="PersonEditModel" /> class.
        /// </summary>
        public PersonEditModel(Person person, IEnumerable<Colour> availableColours)
        {
            this.person = person;
            this.availableColours = availableColours;
        }

        public Person Person
        {
            get
            {
                return person;
            }
        }

        public IEnumerable<Colour> AvailableColours
        {
            get
            {
                return availableColours;
            }
        }
    }
}