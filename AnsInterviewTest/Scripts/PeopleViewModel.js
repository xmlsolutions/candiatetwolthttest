﻿function PeopleViewModel() {
    var self = this;

    self.people = ko.observableArray([]);

    self.GetAllPeople = function () {
        $.getJSON("/api/Person", function (jsonData) {

            for (var i = 0; i < jsonData.length; i++) {
                var person = deserialisePersonFromJSON(jsonData[i]);
                self.people.push(person);
            }
        })
        .done(function () {
            console.log("second success");
        })
        .fail(function (jqxhr, textStatus, error) {
            console.log("Request Failed: " + error);
            alert(error);
        });
    };
    
    self.currentPerson = ko.observable();

    self.GetPerson = function (model) {
        $.getJSON("/api/Person/" + model.id(), function (jsonData) {
           })
          .done(function (jsonData) {
              var person = deserialisePersonFromJSON(jsonData.Person);
              var availableColours = deserialiseAvailableColours(jsonData);

              self.currentPerson(person);
              $('#content').load("/Home/Edit", function () {
                  var personViewModel = new PersonViewModel(self.currentPerson(), availableColours);
                  ko.cleanNode($("#content")[0]);
                  ko.applyBindings(personViewModel, $("#content")[0]);
              });
          })
          .fail(function (jqxhr, textStatus, error) {
              console.log("Request Failed: " + error);
              alert(error);
          });
    };

    self.EditPerson = function (model, event) {
        self.GetPerson(model);
    };

    function deserialisePersonFromJSON(jsonData) {
        var person = new Person(
            jsonData.PersonId,
            jsonData.FirstName,
            jsonData.LastName,
            jsonData.IsAuthorised,
            jsonData.IsEnabled
        );

        $.each(jsonData.Colours, function (index, colour) {
            person.colours.push(new Colour(colour.ColourId, colour.Name, true));
        });

        return person;
    }

    function deserialiseAvailableColours(jsonData) {
        var theColours = [];

        $.each(jsonData.AvailableColours, function (index, colour) {
            theColours.push(new Colour(colour.ColourId, colour.Name, false));
        });

        return theColours;
    }
}