﻿function Colour(id, name, isSelected) {
    return {
        id: ko.observable(id),
        name: ko.observable(name),
        isSelected: ko.observable(isSelected),
        colourId: ko.observable(id)
    };
}