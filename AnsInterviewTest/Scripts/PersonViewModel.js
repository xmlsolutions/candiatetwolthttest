﻿function PersonViewModel(person, availableColours) {
    var self = this;

    self.person = person;

    self.colours = person.colours;

    self.availableColours = ko.observableArray(availableColours);

    // Add the available colours that are not currently in the colours collection.
    $.each(self.availableColours(), function (index, value) {

        var match = ko.utils.arrayFirst(self.colours(), function (item) {
            return value.id() === item.id();
        });

        if (!match) {
            var missing = self.availableColours()[index];
            self.colours.push(missing);
        }

    });

    // Sort the colours collection by name.
    self.colours.sort(function (left, right) { return left.name() == right.name() ? 0 : (left.name() < right.name() ? -1 : 1) });

    self.id = person.id;

    self.firstName = person.firstName;
    self.lastName = person.lastName;

    self.isAuthorised = person.isAuthorised;
    self.isEnabled = person.isEnabled;

    self.fullName = person.fullName;

    self.ToggleAuthorised = function () {
        self.isAuthorised(!self.isAuthorised());
    }

    self.ToggleEnabled = function () {
        self.isEnabled(!self.isEnabled());
    }

    self.ToggleColour = function (colour) {
        colour.isSelected(!colour.isSelected());
    }

    self.CancelEdit = function (model, event) {
        var peopleViewModel = new PeopleViewModel();
        peopleViewModel.GetAllPeople();

        $('#content').load("/Home/Index", function () {
            ko.cleanNode($("#content")[0]);
            ko.applyBindings(peopleViewModel, $("#content")[0]);
        });
    };

    self.UpdatePerson = function (model, event) {
        // Remove the colours that are not selected.
        self.colours.remove(function (item) {
            return !item.isSelected()
        });

        $.ajax({
            url: "api/Person/" + model.person.id(),
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: ko.toJSON(model.person),
            success: function (data) {
                self.CancelEdit(model, event);
            },
            error: function (err) {
                self.CancelEdit(model, event);
            }
        });
    };

}