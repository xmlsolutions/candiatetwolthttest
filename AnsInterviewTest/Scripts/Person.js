﻿function Person(id, firstName, lastName, isAuthorised, isEnabled) {
    var self = this;

    self.colours = ko.observableArray([]);

    function getStatus(aBool) {
        if (aBool == true) {
            return "Yes";
        }
        else {
            return "No";
        }
    }

    return {
        id: ko.observable(id),
        firstName: ko.observable(firstName),
        lastName: ko.observable(lastName),
        isAuthorised: ko.observable(isAuthorised),
        isEnabled: ko.observable(isEnabled),
        colours: self.colours,
        fullName: ko.computed(function () {
            return firstName + " " + lastName;
        }, self),
        isAuthorisedStatus: ko.computed(function () {
            return getStatus(isAuthorised);
        }, self),
        isEnabledStatus: ko.computed(function () {
            return getStatus(isEnabled);
        }, self),
        isPalindromeStatus: ko.computed(function () {
            var forwards = firstName.toLowerCase() + lastName.toLowerCase();
            var backwards = forwards.split('').reverse().join('');

            return getStatus(forwards == backwards);
        }, self),
        colourList: ko.computed(function () {
            var extractedColours = [];
        
            ko.utils.arrayForEach(self.colours(), function (colour) {
                extractedColours.push(colour.name());
            });

            return extractedColours.join(", ");
        }, self)
    };
}
