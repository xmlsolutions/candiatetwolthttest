﻿using AnsInterviewTest.DataLayer;
using AnsInterviewTest.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AnsInterviewTest.Controllers
{
    public class PersonController : ApiController
    {
        private TechTestEntities dbContext = new TechTestEntities();

        // GET api/People
        public IEnumerable<Person> GetPeople()
        {
            IEnumerable<Person> people = dbContext.People.OrderBy(p => p.FirstName).ThenBy(p => p.LastName);
            foreach (Person person in people)
            {
                person.Colours = person.Colours.OrderBy(c => c.Name).ToList<Colour>();
            }

            return people;
        }

        // GET api/People/5
        public PersonEditModel GetPerson(int id)
        {
            Person person = dbContext.People.Find(id);
            IEnumerable<Colour> availableColours = dbContext.Colours.OrderBy(c => c.Name);

            return new PersonEditModel(person, availableColours);
        }

        // PUT api/People/4
        public HttpResponseMessage PutPerson(int id, Person person)
        {
            Person originalPerson = dbContext.People.Find(id);

            dbContext.People.Attach(originalPerson);

            originalPerson.IsAuthorised = person.IsAuthorised;
            originalPerson.IsEnabled = person.IsEnabled;

            foreach (Colour colour in person.Colours)
            {
                dbContext.Colours.Attach(colour);
            }

            // Add any new colours to the collection.
            foreach (Colour colour in person.Colours)
            {
                var originalColour = originalPerson.Colours.SingleOrDefault(c => c.ColourId == colour.ColourId);
                if (originalColour == null)
                {
                    originalPerson.Colours.Add(colour);
                }
                dbContext.Colours.Attach(colour);
            }

            // Remove any old colours from the collection.
            foreach (Colour colour in originalPerson.Colours.ToList())
            {
                if (!person.Colours.Any(c => c.ColourId == colour.ColourId))
                {
                    originalPerson.Colours.Remove(colour);
                }
            }

            dbContext.Entry(originalPerson).State = EntityState.Modified;

            try
            {
                dbContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
